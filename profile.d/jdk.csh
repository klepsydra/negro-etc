setenv J2SDKDIR /usr/lib/jvm/java-12-oracle
setenv J2REDIR /usr/lib/jvm/java-12-oracle
setenv PATH ${PATH}:/usr/lib/jvm/java-11-oracle/bin:/usr/lib/jvm/java-12-oracle/db/bin
setenv JAVA_HOME /usr/lib/jvm/java-12-oracle
setenv DERBY_HOME /usr/lib/jvm/java-12-oracle/db
