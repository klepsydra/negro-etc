


export LANG=en_US.utf8
export LOCALE=$LANG
export LC_ALL=$LANG



#PS1='\h\w\$ (\!) '
#PS1='\h\w\$ \d,\t (\#) '
#PS1='\u@\h\w \d,\t\$ (\!) '
PS1='\u@\h\w \D{%F},\t\$ (\!) '

##echo -e "\33[10;16000]\33[11;100]"
export IGNOREEOF=0
alias ssh='ssh -e ^]'
alias vt='rxvt -T $LOGNAME@$HOSTNAME -sl 10240 -sb -fg white -bg black -font 7x13 -e bash -login&'
alias xt='xterm -T $LOGNAME@$HOSTNAME -sl 10240 -sb -fg white -bg black -font 7x13 -e bash -login&'
#alias e='emacs -bg black -fg white'
#alias es='emacs -fn 6x12 -bg black -fg white'
alias jb='jobs -l'
alias md="mkdir"
alias rd="rmdir"
alias l="ls -lF  --time-style=+%Y-%m-%d,%H-%M-%S"
alias hd='od -Ax -tx1z -v'

pathmunge () {
        if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
           if [ "$2" = "after" ] ; then
              PATH=$PATH:$1
           else
              PATH=$1:$PATH
           fi
        fi
}
        pathmunge /sbin
        pathmunge /usr/sbin
        pathmunge /usr/local/sbin
        pathmunge /usr/local/bin
pathmunge /usr/games after
#pathmunge $HOME/.wine/drive_c/bin after

pathmunge /usr/X11R6/bin after
pathmunge $HOME/bin after

# gcsf login mkUSC  #  /home/master/_cloud/GooogleDrive.mkUSC
pathmunge /home/master/.cargo/bin after


##. /etc/ems
#pathmunge $EMSTOP/sbin after
#pathmunge $EMSTOP/usr/bin before
#pathmunge $EMSTOP/tools before
#pathmunge $EMSTOP/scripts after




export LD_LIBRARY_PATH
export PATH

ulimit -S -c 0 > /dev/null 2>&1
export HISTSIZE=80002
HISTCONTROL=ignoredups:erasedups:ignorespace
export HISTIGNORE="[   ]*:&:bg *:fg *:exit:jb"
export HISTFILESIZE=$HISTSIZE
export PROMPT_COMMAND="history -a"
HISTTIMEFORMAT="%Y-%m-%d_%H-%M-%S " #  history now outputs lines like: 596  2011-03-21_10-04-48 history 33
#HISTTIMEFORMAT="%Y%m%d%H%M%S "
shopt -s histreedit
shopt -s histappend

alias xp='xprop | grep "WM_WINDOW_ROLE\|WM_CLASS\|_NET_WM_PID"' # && echo "WM_CLASS(STRING) = \"NAME\", \"CLASS\""
alias r="fc -s"


#export ftp_proxy=http://localhost:3128
#export http_proxy=http://localhost:3128
export EDITOR=nano

# http://stackoverflow.com/a/6643191/1069375
ANSICON=1

#FUNCTIONS
#function 7zip() { ls -alF $@; 7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on $@.7z $@ && touch -r $@ $@.7z && rm -fv $@ && ls -alF $@.7z;  }
#function 7zipi() { ls -alF $@; 7z a -si -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on $@.7z < $@ && touch -r $@ $@.7z && rm -fv $@ && ls -alF $@.7z; }
function 7zip() { for f in $@; do  ls -alF "$f"; 7za a -t7z -m0=lzma -mx=9 -mfb=64 -md=64m -ms=on "$f.7z" "$f" && touch -r "$f" "$f.7z" && rm -fv "$f" && ls -alF "$f.7z"; done; }
function 7zipi() { for f in $@; do  ls -alF "$f"; 7za a -si -t7z -m0=lzma -mx=9 -mfb=64 -md=64m -ms=on "$f.7z" < "$f" && touch -r "$f" "$f.7z" && rm -fv "$f" && ls -alF "$f.7z"; done; }

#function linescollapse() { for f in $@; do  ls -alF "$f";  tac "$f" |awk ' !x[$0]++' |tac > "$f.$$"  && touch -r "$f" "$f.$$" && ls -alF "$f.$$" && mv -f "$f.$$" "$f"; done; }
function linescollapse() { for f in $@; do  (ls -alF "$f";  tac "$f" |awk ' !x[$0]++' |tac > "$f.$$"  && touch -r "$f" "$f.$$" && ls -alF "$f.$$" && mv -f "$f.$$" "$f";)| column -t; done; }
#ruby -e 'print *readlines.reverse.uniq.reverse'


#xauth list &> /tmp/xauth.$LOGNAME.txt
# awk '{print "xauth add  "$0}' /tmp/xauth.master.txt |sh  # run after sudo -i

# https://stackoverflow.com/questions/1115904/shortest-way-to-swap-two-files-in-bash
function swap()  {  mv -vi "$1" "$2" -b -S .swap  &&  mv -vi "$2".swap "$1"; }
#function swap()         
#{
#    local TMPFILE=tmp.$$
#    mv "$1" $TMPFILE && mv "$2" "$1" && mv $TMPFILE $2
##    mv new old -b && mv old~ new
#}


cmdfu(){ wget -qO - "http://www.commandlinefu.com/commands/matching/$@/$(echo -n "$@" | openssl base64)/plaintext"; }
# cmdfu(){ curl -s "http://www.commandlinefu.com/commands/matching/$@/$(echo -n "$@" | openssl base64)/plaintext"; }



# if git has a problem, try
# unset GIT_SSH
# unset PLINK_PROTOCOL
# unset GIT_SVN



#http://cottidianus.livejournal.com/259515.html
#  `ls --color=always ~/code/gcc-4.5.2 | ansipaste`
# https://raw.github.com/pixelb/scripts/master/scripts/ansi2html.sh
function ansipaste()
{
    $SHELL <(curl https://raw.github.com/pixelb/scripts/master/scripts/ansi2html.sh) | curl -s -S --data-urlencode "txt=`cat`" "http://pastehtml.com/upload/create?input_type=html&result=address"
    echo
}


# http://superuser.com/a/496572/110335
# __wget http://apt-cyg.googlecode.com/svn/trunk/apt-cyg > /usr/bin/apt-cyg && chmod 0755 /usr/bin/apt-cyg
# i just registered to upvote and comment a change that worked for me: - while read line; do - [[ $mark -eq 1 ]] && echo $line + while IFS=$'\n'; read line; do + [[ $mark -eq 1 ]] && printf "%s\n" $line ▒  user311174 Jan 6 at
function __wget() {
    : ${DEBUG:=0}
    local URL=$1
    local tag="Connection: close"
    local mark=0

    if [ -z "${URL}" ]; then
        printf "Usage: %s \"URL\" [e.g.: %s http://www.google.com/]" \
               "${FUNCNAME[0]}" "${FUNCNAME[0]}"
        return 1;
    fi
    read proto server path <<<$(echo ${URL//// })
    DOC=/${path// //}
    HOST=${server//:*}
    PORT=${server//*:}
    [[ x"${HOST}" == x"${PORT}" ]] && PORT=80
    [[ $DEBUG -eq 1 ]] && echo "HOST=$HOST"
    [[ $DEBUG -eq 1 ]] && echo "PORT=$PORT"
    [[ $DEBUG -eq 1 ]] && echo "DOC =$DOC"

    exec 3<>/dev/tcp/${HOST}/$PORT
    echo -en "GET ${DOC} HTTP/1.1\r\nHost: ${HOST}\r\n${tag}\r\n\r\n" >&3
    while read line; do
        [[ $mark -eq 1 ]] && echo $line
        if [[ "${line}" =~ "${tag}" ]]; then
            mark=1
        fi
    done <&3
    exec 3>&-
}


## https://superuser.com/questions/117841/get-colors-in-less-or-more
export LESS='-R'
##export LESSOPEN='|~master/.lessfilter %s'   # Trouble viewing .xz .gz compressed files??
# to list available styles: `pygmentize -L styles`
export PYGMENTIZE_STYLE='paraiso-dark'


# https://gorails.com/setup/ubuntu/14.04
# source ~/.rvm/scripts/rvm
##source /etc/profile.d/rvm.sh

#time curl -L https://get.rvm.io | bash -s stable --autolibs=enabled --ruby --rails
#

##source /usr/local/rvm/scripts/rvm

# https://github.com/postmodern/chruby/blob/master/README.md
##source /usr/local/share/chruby/chruby.sh


