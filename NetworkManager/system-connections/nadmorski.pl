[connection]
id=nadmorski.pl
uuid=85f19603-8f80-43b1-b961-b35b1b582fcb
type=wifi
permissions=user:master:;
secondaries=

[wifi]
mac-address=F4:B7:E2:88:01:ED
mac-address-blacklist=
mac-address-randomization=0
mode=infrastructure
seen-bssids=
ssid=nadmorski.pl

[ipv4]
dns-search=
method=auto

[ipv6]
addr-gen-mode=stable-privacy
dns-search=
method=auto
